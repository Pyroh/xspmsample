//
//  View.swift
//  macOS_Sample
//
//  Created by Pierre TACCHI on 18/04/2019.
//

import Cocoa
import CoreGeometry

class View: NSView {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        let boundsCenter = self.bounds.center
        let tx = 60.0
        let ty = 60.0
        let radius: CGFloat = 8.0
        
        var ltRect, lbRect, rtRect, rbRect: CGRect
        ltRect = CGRect(center: boundsCenter, size: .init(square: 100)).translated(byTx: -tx, ty: ty)
        lbRect = CGRect(center: boundsCenter, size: .init(square: 100)).translated(byTx: -tx, ty: -ty)
        rtRect = CGRect(center: boundsCenter, size: .init(square: 100)).translated(byTx: tx, ty: ty)
        rbRect = CGRect(center: boundsCenter, size: .init(square: 100)).translated(byTx: tx, ty: -ty)
        
        let grey = #colorLiteral(red: 0.2902216315, green: 0.2901752889, blue: 0.2902374566, alpha: 1)
        let yellow = #colorLiteral(red: 1, green: 0.8470588235, blue: 0.1098039216, alpha: 1)
        let blue = #colorLiteral(red: 0.007595625371, green: 0.8083011333, blue: 0.9684422348, alpha: 1)
        let red = #colorLiteral(red: 0.9606321454, green: 0.3172640502, blue: 0.3744366169, alpha: 1)
        
        [(ltRect, grey), (rtRect, yellow), (lbRect, red), (rbRect, blue)].forEach {
            $0.1.setFill()
            NSBezierPath(roundedRect: $0.0, xRadius: radius, yRadius: radius).fill()
        }
    }
    
}
