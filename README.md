![](https://img.shields.io/badge/xspm-dependent-blue.svg) ![](https://img.shields.io/badge/Swift-5.0-orange.svg)

This repository shows how [xspm](https://gitlab.com/Pyroh/xspm) integration is done.

# Prerequisities
- Xcode 10.2
- xspm to be [installed](https://gitlab.com/Pyroh/xspm#installation) and updated.

# How to
- Get the content of this repository. 
- In terminal go to the corresponding folder.
- Run `$ xspm init`
- ...
- Profit

Now you can try and run the iOS and macOS apps in the project.

# Content of the project.
This project contains an iOS application and a macOS application.
Each of these applications use the same dependency. This dependency is managed by [xspm](https://gitlab.com/Pyroh/xspm).

The project is ready to use all you need to do it to fetch the dependencies through xspm.